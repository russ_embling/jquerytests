﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JQueryTests.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        var clickMeWasClicked = false;

        var showModal = function (msg, title) {
            if (msg)
            {
                $('.modal-body').html(msg);
            }
            if (title)
            {
                $('.modal-title').text(title);
            }
            $('#myModal').modal('show');
        };

        var entityMap = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': '&quot;',
            "'": '&#39;',
            "/": '&#x2F;'
        };

        function escapeHtml(string) {
            return String(string).replace(/[&<>"'\/]/g, function (s) {
                return entityMap[s];
            });
        }

        $(document).ready(function () {
            $('#btnClearSession').on('click', function () {
                $('.resetPage').click();
            });
            $('.resetPage').on('click', function () {
                clickMeWasClicked = false;
                $('.unbind').click();
                $('.removeDelegate').click();
                $('.clickme').removeClass('disabled');
                $('.clickme').prop("disabled", false);
            });
            $('.well').each(function () {
                var well = $(this);
                var bind = well.find('.bind');
                var unbind = well.find('.unbind');
                var addDelegate = well.find('.addDelegate');
                var removeDelegate = well.find('.removeDelegate');
                var viewEventHandlers = well.find('.viewEventHandlers');
                var clickMe = well.find('.clickme');

                $(bind).on('click', function () {
                    $(this).parent().find('.clickme').on('click', function () {
                        if (!clickMeWasClicked) {
                            if ($('#chkShowClickPopups').is(":checked")) {
                                showModal('bound event clicked', 'Event Handlers');
                            }
                            if ($('#chkShowClickPopupsSequentially').is(":checked")) {
                                alert('bound event clicked');
                            }
                            $(this).disabled = true;
                            $(this).addClass('disabled');
                            clickMeWasClicked = true;
                            if ($(this).attr('id') != "btnAspClickMe")
                            {
                                // we need to call the possibly disabled btnAspClickMe button
                                $('#btnAspClickMe').unbind('click');
                                // re-enable the asp button temporarily
                                $('#btnAspClickMe').prop("disabled", false);
                                // create a new bind function for the click event
                                $('#btnAspClickMe').on('click', function () {
                                    // set the button back to disabled
                                    $(this).disabled = true;
                                    $(this).addClass('disabled');
                                    // upon completion here, it will fall through to the server side call
                                });
                                // now call the asp button click event
                                $('#btnAspClickMe').click();
                            }
                        }
                        else {
                            showModal("you can only click this button once", 'Warning');
                            return false;
                        }
                    });
                });

                $(unbind).on('click', function () {
                    $(this).parent().find('.clickme').unbind();
                });

                $(removeDelegate).on('click', function () {
                    $(this).parent().off();
                });

                $(addDelegate).on('click', function (e) {
                    $(this).parent().on('click', '.clickme', function () {
                        if ($('#chkShowClickPopups').is(":checked")) {
                            showModal("delegate click event", 'Event Handlers');
                        }
                        if ($('#chkShowClickPopupsSequentially').is(":checked")) {
                            alert('delegate click event');
                        }
                    });
                    e.preventDefault();
                });

                $(viewEventHandlers).on('click', function () {
                    var clickEvents = findEventHandlers('click', $(this).parent().find('.clickme'));
                    var eventCount = 0;
                    var elements = '';
                    if (clickEvents) {
                        $(clickEvents).each(function (i, val) {
                            if (val) {
                                if (val.events) {
                                    eventCount += val.events.length;
                                    if (val.element && val.element['outerHTML'])
                                    {
                                        elements += escapeHtml(val.element['outerHTML'].substring(0, val.element['outerHTML'].indexOf('>') + 1)) + '<br />';
                                    }
                                }
                            }
                        });
                        showModal(eventCount + ' click event handler(s)' + '<br />' + elements, 'Event Handlers');
                    }
                    else {
                        showModal('0 click event handler(s)', 'Event Handlers');
                    }
                    // prevent on-document-ready from running again
                    return false;
                });
            });

            $('#myModal').on('shown.bs.modal', function () {
                $('#myInput').focus();
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <br />
        <b>Title:</b>
        <br />
        Prevent duplicate button clicks (with Jquery) and do only one server-side post back.
        <br />
        <br />
        <b>Overview:</b>
        <br />
        <p>
            In order to prevent the user from clicking a button twice, we'll bind a javascript function to the button, to disable additional clicks. Note: I'm showing two buttons for the example; one is an ASP.NET button (i.e. HTML input control) and the other is a simple HTML HREF control. This example also serves as a teaching aid for how you can set event handlers through both binding and delegation. It's important to understand these core concepts of javascript event handling and how to debug them. 
        </p>
        <b>Notes:</b>
        <ul>
            <li>bind events fire first, then all delegates in the order they were added, then any form submit controls (e.g. &#060;input type='submit'&#062;)</li>
            <li>use JQuery .on() to assign both bind events and delegate events by specifying the correct parameter signature</li>
            <li>use JQuery .off() to turn off delegates</li>
            <li>use JQuery .unbind() to unbind a bound event</li>
            <li>use findHandlersJS to find and debug javascript event handlers</li>
            <li>use preventDefaults(), stopPropagation() and 'return false' correctly</li>
        </ul>
        <b>Online Resources:</b>
        <br />
        <a href="http://www.elijahmanor.com/differences-between-jquery-bind-vs-live-vs-delegate-vs-on/">Differences between JQuery Bind vs Live vs Delegate vs On</a>
        <br />
        <a href="http://fuelyourcoding.com/jquery-events-stop-misusing-return-false/">JQuery events: Stop misusing 'return false'</a>
        <br />
        <a href="https://blinkingcaret.wordpress.com/2014/01/17/quickly-finding-and-debugging-jquery-event-handlers/#findHandlersJS">Quickly finding and debugging JQuery event handlers</a>
        <br />
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#myModal">
          Launch demo modal
        </button>
        <br />
        <a href="#" class="btn btn-default resetPage">Reset Page</a> 
        <asp:Button ID="btnClearSession" runat="server" Text="Clear Session & Reset Page" CssClass="btn btn-default" OnClick="btnClearSession_Click" />
        <div style="display:none;"><asp:CheckBox ID="chkShowClickPopups" runat="server" ClientIDMode="Static" />&nbsp;Show Click Popups</div>
        <asp:CheckBox ID="chkShowClickPopupsSequentially" runat="server" ClientIDMode="Static" />&nbsp;Show Click Popups Sequentially
        <br />
        <br />
        <div class="well">
            <asp:Button ID="btnAspClickMe" Text="ASP.NET Button Click Me" CssClass="btn btn-default clickme" runat="server" ClientIDMode="Static" OnClick="btnAspClickMe_Click" /><br />
            <br />
            <a href="#" class="btn btn-primary bind">Bind 'Prevent Duplicate' Function</a>
            <a href="#" class="btn btn-primary unbind">Unbind</a>
            <a href="#" class="btn btn-primary addDelegate">Add Delegate</a>
            <a href="#" class="btn btn-primary removeDelegate">Remove Delegate(s)</a>
            <a href="#" class="btn btn-primary viewEventHandlers">View Event Handlers</a><br />
        </div>
        <div class="well">
            <a runat="server" id="btnHrefClickMe" href="#" class="btn btn-default clickme">Href Click Me</a><br />
            <br />
            <a href="#" class="btn btn-primary bind">Bind 'Prevent Duplicate' Function</a>
            <a href="#" class="btn btn-primary unbind">Unbind</a>
            <a href="#" class="btn btn-primary addDelegate">Add Delegate</a>
            <a href="#" class="btn btn-primary removeDelegate">Remove Delegate(s)</a>
            <a href="#" class="btn btn-primary viewEventHandlers">View Event Handlers</a><br />
        </div>
        <br />
        <asp:Label ID="lblMessage" runat="server" ClientIDMode="Static" EnableViewState="false" ForeColor="Green"></asp:Label><br />
        <asp:Label ID="lblError" runat="server" ClientIDMode="Static" EnableViewState="false" ForeColor="Red"></asp:Label><br />
        <br />
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              </div>
              <div class="modal-body">
                ...
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary hidden">Save changes</button>
              </div>
            </div>
          </div>
        </div>
    </div>
</asp:Content>
