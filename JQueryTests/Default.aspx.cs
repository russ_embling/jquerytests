﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JQueryTests
{
    [Serializable]
    public class ServerSideEventTimeStamp
    {
        public DateTime Date
        {
            get;
            set;
        }
        public string Message
        {
            get;
            set;
        }
    }
    public partial class Default : System.Web.UI.Page
    {
        public List<ServerSideEventTimeStamp> ServerSideEventTimeStamps
        {
            get
            {
                if (Session["ServerSideEventTimeStamps"] == null)
                {
                    List<ServerSideEventTimeStamp> list = new List<ServerSideEventTimeStamp>();
                    Session["ServerSideEventTimeStamps"] = list;
                }
                return (List<ServerSideEventTimeStamp>)Session["ServerSideEventTimeStamps"];
            }
            set
            {
                Session["ServerSideEventTimeStamps"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindServerSideEventTimeStamps();
            }
        }

        private void BindServerSideEventTimeStamps()
        {
            lblMessage.Text = string.Join("<br />", ServerSideEventTimeStamps.OrderByDescending(x => x.Date).Select(y => y.Message).ToArray());
        }

        protected void btnAspClickMe_Click(object sender, EventArgs e)
        {
            try
            {
                Thread.Sleep(2000);
                ServerSideEventTimeStamps.Add(new ServerSideEventTimeStamp()
                {
                    Date = DateTime.Now,
                    Message = string.Format("{0}: server-side click event received", DateTime.Now.ToString())
                });
                
                BindServerSideEventTimeStamps();
                
                btnAspClickMe.Enabled = false;
                btnHrefClickMe.Attributes.Add("Enabled", "false");
                btnHrefClickMe.Attributes.Add("class", "btn btn-default clickme disabled");
            }
            catch (Exception ex)
            {
                lblMessage.Text = string.Format("{0}: Error: {1}", DateTime.Now.ToString(), ex.Message);
            }
        }

        protected void btnClearSession_Click(object sender, EventArgs e)
        {
            try
            {
                Session["ServerSideEventTimeStamps"] = null;

                btnAspClickMe.Enabled = true;
                btnHrefClickMe.Attributes.Remove("Enabled");
                btnHrefClickMe.Attributes.Add("class", "btn btn-default clickme");

                BindServerSideEventTimeStamps();
            }
            catch (Exception ex)
            {
                lblMessage.Text = string.Format("{0}: Error: {1}", DateTime.Now.ToString(), ex.Message);
            }
        }

    }
}