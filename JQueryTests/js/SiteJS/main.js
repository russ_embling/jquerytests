/* ----- Javascript ----- */

/* ==========================================================================
   Global Variables
   ========================================================================== */
var compareWidth; //previous width used for comparison
var detector; 	//the element used to compare changes
				// for performance, I store the detector element into a variable to avoid
				// running a DOM query every time we need to test the change conditions



/* ==========================================================================
   Methods
   ========================================================================== */
detectMediaQuery = function (w) {
    if (w == 976)
        goDesktop();
    else if (w == 750)
        goTablet();
    else
        goMobile();

}

realignPageCycleControls = function() {
	
	if ($(window).width() < 1132) {
		$('.pageCyclePrevBtn').css({ 'left': '-100px' });
		$('.pageCycleNextBtn').css({ 'right': '-100px' });
	} else {
		$('.pageCyclePrevBtn').css({ 'left': '0' });
		$('.pageCycleNextBtn').css({ 'right': '0' });
	}
}

goDesktop = function() {

	// check if secondary nav is too long and is wrapping to two lines
	if ($('.secondaryNav').height() > 65)
		$('.secondaryNav li:first-child').css({ 'float': 'none', 'text-align': 'left'}) 
	
	// put the search form back into the top bar
	var search = $('#topBarSearchForm');
	search.remove();
	$('.topBarInner').append(search);
	
	// add click handler for MyChart button
	$('#myChartBtn').off('click', myChartBtnClickHandler).on('click', myChartBtnClickHandler);
	
	// remove click handler for mobile secondary nav opener
	$('.mainNavWrap').show();
	$('.mainNav .topBorder').off('click', mobileMenuOpenClickHandler);
	
	// override any JS on these elements
	$('.tertiaryNav, .quaternaryNav').show();
	
	// add functionality for content sliders (page specific JS file)
	if ($.isFunction(window.addContentSliders))
		addContentSliders();
	
	// show full footer, remove click handler
	$('.footerLinks .category ul').show();
	$('.footerLinks .headerContainer h4').off('click', footerCategoryClickHandler);
	
}

goTablet = function() {

	// put the search form back into the top bar
	var search = $('#topBarSearchForm');
	search.remove();
	$('.topBarInner').append(search);
	
	// remove click handler for MyChart button
	$('#myChartBtn').off('click', myChartBtnClickHandler);
	$('.topBarNav .loginWrap').hide();
	
	// remove click handler for mobile secondary nav opener
	$('.mainNavWrap').show();
	$('.mainNav .topBorder').off('click', mobileMenuOpenClickHandler);
	
	// override any JS on these elements
	$('.tertiaryNav, .quaternaryNav').show();
	
	// add functionality for content sliders (page specific JS file)
	if ($.isFunction(window.addContentSliders))
		addContentSliders();
		
	// show mobile footer, add click handler
	$('.footerLinks .category ul').hide();
	$('.footerLinks .headerContainer h4').off('click', footerCategoryClickHandler).on('click', footerCategoryClickHandler);
	
}

goMobile = function() {
	
	
	
	// remove click handler for MyChart button
	$('#myChartBtn').off('click', myChartBtnClickHandler);
	$('.topBarNav .loginWrap').hide();
	
	// add click handler for mobile secondary nav opener
	$('.mainNavWrap').hide();
	
	$('.navMenuBtn').off('click').on('click', function(){
	    $('#mobileSecondarNavId').hide();
		$('.mainNavWrap').slideToggle('fast');
	});
	
	$('.mainNav .topBorder').on('click', mobileMenuOpenClickHandler);
	
	// override any JS on these elements
	$('.tertiaryNav, .quaternaryNav').hide();
	
	// remove functionality for content sliders (page specific JS file)
	if ($.isFunction(window.addContentSliders))
		removeContentSliders();
	
	// show mobile footer, add click handler
	$('.footerLinks .category ul').hide();
	$('.footerLinks .headerContainer h4').off('click', footerCategoryClickHandler).on('click', footerCategoryClickHandler);
}



/* ==========================================================================
   Event Handlers
   ========================================================================== */
function myChartBtnClickHandler() {
	var clicks = $(this).data('clicks');
	if (clicks) {
		$('.topBarNav .loginWrap').animate({ width: 'hide' });
	} else {
		$('.topBarNav .loginWrap').animate({ width: 'show' }).css('display', 'table-cell');
	}
	$(this).data("clicks", !clicks);

	return false;
}

function mobileMenuOpenClickHandler() {
    if ($("#mobileSecondarNavId, #mobileSubcategories, #mobileThirdlyLinks").is(":visible")) {
        return;
    }

	var arr = $(this).parent('li').attr('class').split(" ");
	var section = arr[0];
	$.each($('#mobileSecondarNavId'), function (index, nav) {
		if ($(this).hasClass(section)) {
			$(this).show().css({ 'left': '100%' }).animate({
				left: '0'
			}, 500);
		}
	});
}

function mobileMenuCloseClickHandler() {
    $(this).parent('#mobileSecondarNavId').animate({
		left: '100%'
	}, 500, function(){
		$(this).hide();
	});
}

function footerCategoryClickHandler() {
	var that = $(this),
		menu = that.parent('.headerContainer').siblings('ul'),
		others = that.parents('.category').siblings('.category').children('ul');
	
	others.slideUp('fast');
	menu.slideToggle('fast');
}

function pageCycleNextClickHandler(){
	$(this).pageCycle('right');
	return false;
}

function pageCyclePrevClickHandler(){
	$(this).pageCycle('left');
	return false;
}

function tertiaryNavLinkClickHandler() {
	if ($('.tertiaryNav').css('display') == 'none') 
		$(this).css('background', 'url(img/icons/icon_arrow_white_down.png) 5px 7px no-repeat');
	else
		$(this).css('background', 'url(img/icons/icon_arrow_white_right.png) 7px 5px no-repeat');
		
	$('.tertiaryNav').slideToggle();
	return false;
}

function quaternaryNavLinkClickHandler() {
	if ($('.quaternaryNav').css('display') == 'none') 
		$(this).css('background', 'url(img/icons/icon_arrow_white_down.png) 5px 7px no-repeat');
	else
		$(this).css('background', 'url(img/icons/icon_arrow_white_right.png) 7px 5px no-repeat');
	
	$('.quaternaryNav').slideToggle();
	return false;
}



/* ==========================================================================
   Document Ready Code
   ========================================================================== */
$(document).ready(function() {

	//set the initial values
    detector = $('#changeTarget');
    compareWidth = detector.width();
    
    $(window).resize(function(){
        //compare everytime the window resize event fires
        if(detector.width()!=compareWidth){

            //a change has occurred so update the comparison variable
            compareWidth = detector.width();
            
            detectMediaQuery(detector.width());
        }
        
        realignPageCycleControls();

    });
    
    detectMediaQuery(detector.width());
    realignPageCycleControls();

	$('#topMyChartUsername, #topBarSearchInput, #loginHomeUsername').hideInitValue();

	$('#mobileSecondarNavId span').click(mobileMenuCloseClickHandler);
    
    $('.pageCycleNextBtn').click(pageCycleNextClickHandler);
	$('.pageCyclePrevBtn').click(pageCyclePrevClickHandler);
	
	$('.tertiaryNavLink').click(tertiaryNavLinkClickHandler);
	$('.quaternaryNavLink').click(quaternaryNavLinkClickHandler);
	
	
	// Initial password value management
	// On DOM ready, hide the real password
	$('.passReal').hide();
	
	// Show the fake pass (because JS is enabled)
	$('.passFake').show();
	
	// On focus of the fake password field
	$('.passFake').focus(function(){
		$(this).hide(); //  hide the fake password input text
		$(this).siblings('.passReal').show().focus(); // and show the real password input password
	});
	
	// On blur of the real pass
	$('.passReal').blur(function(){
		if($(this).val() == ""){ // if the value is empty, 
			$(this).hide(); // hide the real password field
			$(this).siblings('.passFake').show(); // show the fake password
		}
		// otherwise, a password has been entered,
		// so do nothing (leave the real password showing)
	});
});
