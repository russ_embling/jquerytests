$(window).load(function () {
    $('.flexslider').flexslider({
        animation: "slide",
        start: function (slider) {
            $('body').removeClass('loading');
            // Find the first image in the slider. It doesn't matter which one we find, they are all the same height
            var currentSlide = $('.flex-active-slide img')[0];

            // Get the height of that image we just found
            var currentSlideHeight;
            if (currentSlide.currentStyle) {
                currentSlideHeight = currentSlide.currentStyle.height;
            }
            else {
                currentSlideHeight = currentSlide.height;
            } 

            // Set the css of the navigation arrows so that they are at the bottom edge of the image
            $('.flex-direction-nav a').css("top", currentSlideHeight);    
        }
    });

    $(window).resize(function () {
        // Find the first image in the slider. It doesn't matter which one we find, they are all the same height
        var currentSlide = $('.flex-active-slide img')[0];
        if (currentSlide != null) {
            // Get the height of that image we just found
            var currentSlideHeight;
            if (currentSlide.currentStyle) {
                currentSlideHeight = currentSlide.currentStyle.height;
            }
            else {
                currentSlideHeight = currentSlide.height;
            }

            // Set the css of the navigation arrows so that they are at the bottom edge of the image
            $('.flex-direction-nav a').css("top", currentSlideHeight);
        }
    });
});