/* ----- Javascript ----- */

/* ==========================================================================
   Global Variables
   ========================================================================== */
var rawCTASlider,
	rawArticleSlider,
	rawCalendarSlider;


/* ==========================================================================
   Methods
   ========================================================================== */
closeCalendarDetail = function() {
	$('.calendarSliderDetail').remove();
}

addContentSliders = function() {

	$('.ctaSlider').remove();
	$(".ctaSliderWrap").append("<div class='ctaSlider'></div>");
    $(".ctaSlider").html(rawCTASlider);
	
	// call to action slider on home page
	$('.ctaSlider').flexslider({
		animation			: "fade",
		directionNav		: false,
		controlsContainer	: ".ctaContent",
    	manualControls		: ".ctaControl" 
	});
	
	$('.calendarSlider').remove();
	$(".calendarSliderWrap").append("<div class='calendarSlider'></div>");
    $(".calendarSlider").html(rawCalendarSlider);
    $('.calendarSlider .slides li').on('click', calendarSliderLIClickHandler);
	
	// article slider on home page
	$('.calendarSlider').flexslider({
		animation			: "slide",
		slideshow			: false,
		controlNav			: false,
		animationLoop		: false, 
		itemWidth			: 122,
		move 				: 1,
		controlsContainer	: ".calendarControls",
		before				: closeCalendarDetail
	});
	
	$('.articleSlider').remove();
	$(".articleSliderInner").append("<div class='articleSlider'></div>");
    $(".articleSlider").html(rawArticleSlider);
	
	// article slider on home page
	$('.articleSlider').flexslider({
		animation			: "slide",
		slideshow			: false,
		controlNav			: false,
		animationLoop		: false, 
		itemWidth			: 214,
		itemMargin			: 20,
		controlsContainer	: ".articleControls"
	});
}

removeContentSliders = function() {

	$('.ctaSlider').remove();
	$(".ctaSliderWrap").append("<div class='ctaSlider'></div>");
    $(".ctaSlider").html(rawCTASlider);

	$('.calendarSlider').remove();
	$(".calendarSliderInner").append("<div class='calendarSlider'></div>");
    $(".calendarSlider").html(rawCTASlider);
    $('.calendarSlider .slides li').off('click', calendarSliderLIClickHandler);

	$('.articleSlider').remove();
	$(".articleSliderInner").append("<div class='articleSlider'></div>");
    $(".articleSlider").html(rawArticleSlider);	
}


/* ==========================================================================
   Event Handlers
   ========================================================================== */
function calendarSliderLIClickHandler(){
	
	closeCalendarDetail();
	
	var w = 162 - 122; // width of detail box - width of date slide
	var offset = 0;
	var p = $(this).position().left;
	var l = p + offset - (w / 2);
	

	var $obj = $(this).children('div').clone();
	$obj.addClass('calendarSliderDetail')
		.removeClass('noDisplay')
		.css({
			top	 : '-45px',
			left : l
		})
		.click(closeCalendarDetail);
	
	$(this).parents('.calendarSlider').append($obj);
}



/* ==========================================================================
   Document Ready Code
   ========================================================================== */
$(document).ready(function() {
    
    rawCTASlider = $('.ctaSlider').html();
    rawArticleSlider = $('.articleSlider').html();
    rawCalendarSlider = $('.calendarSlider').html();
	
});
