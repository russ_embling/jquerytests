/* ----- Javascript ----- */

/* ==========================================================================
   Global Variables
   ========================================================================== */
var rawArticleSlider;


/* ==========================================================================
   Methods
   ========================================================================== */

addContentSliders = function() {

	$('.articleSlider').remove();
	$(".articleSliderInner").append("<div class='articleSlider'></div>");
    $(".articleSlider").html(rawArticleSlider);
	
	// article slider on home page
	$('.articleSlider').flexslider({
		animation			: "slide",
		slideshow			: false,
		controlNav			: false,
		animationLoop		: false, 
		itemWidth			: 214,
		itemMargin			: 20,
		controlsContainer	: ".articleControls"
	});
}

removeContentSliders = function() {

	$('.articleSlider').remove();
	$(".articleSliderInner").append("<div class='articleSlider'></div>");
    $(".articleSlider").html(rawArticleSlider);	
}


/* ==========================================================================
   Document Ready Code
   ========================================================================== */
$(document).ready(function() {
    
    rawArticleSlider = $('.articleSlider').html();
	
});
